__author__ = 'ryoh'

from random import random

class Player:
    _score = 0
    _is_server = False
    _winning_probability = 0

    def __init__(self, winning_probability: float, is_server: bool):
        self._winning_probability = winning_probability
        self._is_server = is_server

    def hit_ball(self) -> bool:
        """
        @return: Whether or not the player successfully hit the ball
        """
        if random() < self._winning_probability:
            return True
        else:
            return False

    def serve_ball(self) -> None:
        """
        This is only a symbolic function for serving the ball.
        Does nothing
        """
        if self._is_server:
            pass

    def get_score(self) -> int:
        """
        @return: This players current score
        """
        return self._score

    def reset_score(self) -> None:
        """
        Rest player's score to 0
        """
        self._score = 0

    def increment_score(self) -> None:
        """
        Increment players score by 1
        """
        self._score += 1

    def is_serving(self, is_server: bool = None) -> bool:
        """

        @param is_server:
        @return: Whether or not player is the current server
        """
        if is_server is None:
            return self._is_server
        else:
            self._is_server = is_server
            return self._is_server