__author__ = 'ryoh'

import Player

class RacquetballGame:
    _player_a = None
    _player_b = None
    _MAX_SCORE = 15

    def __init__(self, player_a: Player, player_b: Player):
        self.player_a = player_a
        self.player_b = player_b

    def start(self):
        self._reset()

        while not self._game_over():

            if self.player_a.is_serving():
                player1 = self.player_a
                player2 = self.player_b
                player1.serve_ball()
            else:
                player1 = self.player_b
                player2 = self.player_a
                player1.serve_ball()

            while True:
                if not player2.hit_ball():
                    if player2.is_serving():
                        player2.is_serving(False)
                        player1.is_serving(True)
                    player1.increment_score()
                    break

                player1, player2 = player2, player1

        if self.player_a.get_score() > self.player_b.get_score():
            ret = self.player_a
        else:
            ret = self.player_b

        return ret

    def _reset(self):
        self.player_a.reset_score()
        self.player_b.reset_score()
        self.player_a.is_serving(True)
        self.player_b.is_serving(False)

    def _game_over(self) -> bool:
        if self.player_a.get_score() < self._MAX_SCORE:
            if self.player_b.get_score() < self._MAX_SCORE:
                return True
        else:
            return False