from unittest import TestCase

__author__ = 'ryoh'

from RacquetballGame import RacquetballGame
from Player import Player

class TestRacquetballGame(TestCase):

    def test_start(self):
        print("Testing with 100% probability")
        player_a = Player(1, True)
        player_b = Player(0, False)

        game = RacquetballGame(player_a, player_b)
        winner = game.start()

        self.assertEqual(winner, player_a)

        print("Testing with player_a having 70% player_b having 43%")
        player_a = Player(.70, True)
        player_b = Player(.43, False)

        game = RacquetballGame(player_a, player_b)
        winner = game.start()

        losing_player = None

        if winner == player_a:
            losing_player = player_b

        if winner == player_b:
            losing_player = player_a

        self.assertGreater(winner.get_score(), losing_player.get_score())