__author__ = 'ryoh'

from Player import Player
from RacquetballGame import RacquetballGame

class RacquetballSimulator:
    player_a = None
    player_b = None

    player_a_games_won = 0
    player_b_games_won = 0

    game = None

    number_of_games = 0

    def __init__(self, player_a_winning_probability: float,
                 player_b_winning_probability: float, number_of_games: int):

        self.player_a = Player(player_a_winning_probability, True)
        self.player_b = Player(player_b_winning_probability, False)

        self.game = RacquetballGame(self.player_a, self.player_b)
        self.number_of_games = number_of_games

    def start(self) -> None:
        for x in range(self.number_of_games):
            winner = self.game.start()
            if winner == self.player_a:
                self.player_a_games_won += 1
            else:
                self.player_b_games_won += 1

    def display_results(self) -> None:
        player_a_win_percentage = "({0}%)".format(
            (self.player_a_games_won / self.number_of_games) * 100)

        player_b_win_percentage = "({0}%)".format(
            (self.player_b_games_won / self.number_of_games) * 100)

        print("\nGames Simulated:", self.number_of_games)
        print("Wins for A:", self.player_a_games_won, player_a_win_percentage)
        print("Wins for B:", self.player_b_games_won, player_b_win_percentage)

def main():
    player_a_win_chance = float(input("What is the prob. player A wins a serve? "))
    player_b_win_chance = float(input("What is the prob. player B wins a serve? "))
    number_of_games = int(input("How many games to simulate? "))

    simulator = RacquetballSimulator(player_a_win_chance,
                                     player_b_win_chance, number_of_games)
    simulator.start()
    simulator.display_results()

if __name__ == '__main__':
    main()